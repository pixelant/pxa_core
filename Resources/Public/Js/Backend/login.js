(function( p, undefined ) {
	p.radioButtonSelected = function(e) {
		var radioButton = e.target || e.srcElement;
		var radioButtonValue = radioButton.value;
		var select = document.getElementById('t3-interfaceselector');
		for(var i, j = 0; i = select.options[j]; j++) {
			if(i.value == radioButtonValue) {
				select.selectedIndex = j;
				break;
			}
		}
	}
}( window.pxa = window.pxa || {} ));