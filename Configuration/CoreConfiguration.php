<?php
$localConfiguration = require PATH_site . 'typo3conf/LocalConfiguration.php';

/*--------------------------- Merged Configuration --------------------------*/
/*
	This configuration will be overwritten by LocalConfiguration.php if array 
	key exists there.
*/
$defaultConfiguration = array(
	'BE' => array(
		'loginSecurityLevel' => 'rsa',
	),
	'EXT' => array(
		'extConf' => array(
			'saltedpasswords' => 'a:7:{s:20:"checkConfigurationFE";s:1:"0";s:20:"checkConfigurationBE";s:1:"0";s:3:"FE.";a:5:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:31:"tx_saltedpasswords_salts_phpass";s:11:"forceSalted";s:1:"0";s:15:"onlyAuthService";s:1:"0";s:12:"updatePasswd";s:1:"1";}s:3:"BE.";a:5:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:31:"tx_saltedpasswords_salts_phpass";s:11:"forceSalted";s:1:"0";s:15:"onlyAuthService";s:1:"0";s:12:"updatePasswd";s:1:"1";}s:21:"checkConfigurationFE2";s:1:"0";s:21:"checkConfigurationBE2";s:1:"0";s:8:"__meta__";N;}',
			'pxa_support' => 'a:2:{s:10:"supportUrl";s:40:"http://www.pixelant.se/support/helpdesk/";s:9:"manualUrl";s:26:"http://www.typo3manual.se/";}',
			'ed_pixlr' => 'a:2:{s:13:"defaultEditor";s:7:"express";s:11:"userEnabled";s:1:"1";}',
			'pxa_core' => 'a:4:{s:14:"newsDetailPids";s:6:"45,102";s:20:"newsCategoryViewPids";s:6:"94,100";s:15:"newsTagViewPids";s:5:"95,99";s:16:"newsDateViewPids";s:6:"93,101";}',
			'realurl' => 'a:5:{s:10:"configFile";s:26:"typo3conf/realurl_conf.php";s:14:"enableAutoConf";s:1:"0";s:14:"autoConfFormat";s:1:"0";s:12:"enableDevLog";s:1:"0";s:19:"enableChashUrlDebug";s:1:"0";}',
		),
	),
	'FE' => array(
		'loginSecurityLevel' => 'rsa',
		'hidePagesIfNotTranslatedByDefault' => '1',
	),
);
/*-------------------------- Extended Configuration -------------------------*/
/*
	This configuration will overwrite LocalConfiguration.php if array key exists there
	This configuration is not overwritable from LocalConfiguration.
*/

$extendedConfiguration = array(
	'INSTALL' => array(
		'wizardDone' => array(
			'TYPO3\CMS\Install\CoreUpdates\InstallSysExtsUpdate' => '["info","perm","func","filelist","cshmanual","recycler","t3editor","reports","scheduler"]',
		),
	),
);

	// Try to unset extension configuration from localConfiguration if it is empty and a defaultConfiguration exists.
foreach ($defaultConfiguration['EXT']['extConf'] as $key => $data) {
		// Unserialize settings in defaultConfiguration for extension
	$defalultConfigurationSettings = unserialize($data);
		// If the ext have setting in localConfiguration
	if (isset($localConfiguration['EXT']['extConf'][$key])) {
			// Unserialize settings in localConfiguration for extension
		$localConfigurationSettings = unserialize($localConfiguration['EXT']['extConf'][$key]);
			// If there are no settings in localConfigurationSettings for extension and there are settings in the defaultConfiguration
		if (count($localConfigurationSettings) == 0 && count($defalultConfigurationSettings) > 0) {
				// unset localConfigurationSettings for ext
			unset($localConfiguration['EXT']['extConf'][$key]);
		}
	}
}

$mergedConfiguration = TYPO3\CMS\Core\Utility\GeneralUtility::array_merge_recursive_overrule($defaultConfiguration, $localConfiguration);
$mergedConfiguration = TYPO3\CMS\Core\Utility\GeneralUtility::array_merge_recursive_overrule($mergedConfiguration, $extendedConfiguration);

$GLOBALS['TYPO3_CONF_VARS'] = TYPO3\CMS\Core\Utility\GeneralUtility::array_merge_recursive_overrule($GLOBALS['TYPO3_CONF_VARS'], $mergedConfiguration);


	// Clean up
unset($defaultConfiguration);
unset($extendedConfiguration);
unset($localConfiguration);
unset($mergedConfiguration);

?>