<?php

// News dates (archive)

$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['glomt'] = array(
	array(
       	'GETvar' => 'tx_felogin_pi1[forgot]',
       	'valueMap' => array(
			'losenord' => '1',
		),
       	'noMatch' => 'bypass',
	),
);

$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['aterstall'] = array(
	array(
       	'GETvar' => 'tx_felogin_pi1[user]',
	),
	array(
       	'GETvar' => 'tx_felogin_pi1[forgothash]',
	),
);

?>