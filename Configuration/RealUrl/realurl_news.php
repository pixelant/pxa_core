<?php
$extConf = unserialize(($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['pxa_core']));

	// Fixed single view pages for page uids set in extension manager, so we get rid of the "artikel" param in path
$commaSeparatedNewsDetailPids = $extConf['newsDetailPids'] ? $extConf['newsDetailPids'] : '45';
$newsDetailPids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $commaSeparatedNewsDetailPids);
$newsConf = array(
	array(
		'GETvar' => 'tx_news_pi1[controller]',
		'noMatch' => 'bypass',
	),
	array(
		'GETvar' => 'tx_news_pi1[action]',
		'noMatch' => 'bypass',
	),
	array(
		'GETvar' => 'tx_news_pi1[news]',
		'lookUpTable' => array(
			'table' => 'tx_news_domain_model_news',
			'id_field' => 'uid',
			'alias_field' => 'title',
			'addWhereClause' => ' AND NOT deleted',
			'useUniqueCache' => 1,
			'useUniqueCache_conf' => array(
				'strtolower' => 1,
				'spaceCharacter' => '-',
				'encodeTitle_userProc' => 'Pixelant\PxaCore\Hooks\URLEncode->encodeTitle',
			),
			'autoUpdate' => 1,
			'expireDays' => 30,
				// language support (translated urls)
			'languageGetVar' => 'L',
			'languageExceptionUids' => '',
			'languageField' => 'sys_language_uid',
			'transOrigPointerField' => 'l10n_parent',
				// let's enable proper 404 error handling
				// not to get "empty blog/article/some-random-stuff/" page
			'enable404forInvalidAlias' => '1',
		),
	),
);

	// preserve already set fixedPostVars and add our page uids
if(is_array($TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'])){
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] += array_fill_keys($newsDetailPids, $newsConf);
} else {
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] = array_fill_keys($newsDetailPids, $newsConf);
}


	// news tags 
$commaSeparatedNewsTagViewPids = $extConf['newsTagViewPids'] ? $extConf['newsTagViewPids'] : '98';
$newsTagViewPids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $commaSeparatedNewsTagViewPids);
$tagsConf = array(
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][tags]',
		'lookUpTable' => array(
			'table' => ' tx_news_domain_model_tag',
			'id_field' => 'uid',
			'alias_field' => 'title',
			'addWhereClause' => ' AND NOT deleted',
			'useUniqueCache' => 1,
			'useUniqueCache_conf' => array(
				'strtolower' => 1,
				'spaceCharacter' => '-',
				'encodeTitle_userProc' => 'Pixelant\PxaCore\Hooks\URLEncode->encodeTitle',
			),
		),
	),
);

	// preserve already set fixedPostVars and add our page uids
if(is_array($TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'])){
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] += array_fill_keys($newsTagViewPids, $tagsConf);
} else {
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] = array_fill_keys($newsTagViewPids, $tagsConf);
}


	// News categories
$commaSeparatedNewsCategoryViewPids = $extConf['newsCategoryViewPids'] ? $extConf['newsCategoryViewPids'] : '97';
$newsCategoryViewPids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $commaSeparatedNewsCategoryViewPids);
$categoryConf = array(
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][categories]',
		'lookUpTable' => array(
			'table' => 'sys_category',
			'id_field' => 'uid',
			'alias_field' => 'title',
			'addWhereClause' => ' AND NOT deleted',
			'useUniqueCache' => 1,
			'useUniqueCache_conf' => array(
				'strtolower' => 1,
				'spaceCharacter' => '-',
				'encodeTitle_userProc' => 'Pixelant\PxaCore\Hooks\URLEncode->encodeTitle',
			),
				// language support (translated urls)
			'languageGetVar' => 'L',
			'languageExceptionUids' => '',
			'languageField' => 'sys_language_uid',
			'transOrigPointerField' => 'l10n_parent',
		),
	)
);
	// preserve already set fixedPostVars and add our page uids
if(is_array($TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'])){
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] += array_fill_keys($newsCategoryViewPids, $categoryConf);
} else {
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] = array_fill_keys($newsCategoryViewPids, $categoryConf);
}


	// News by dates
$commaSeparatedNewsDateViewPids = $extConf['newsDateViewPids'] ? $extConf['newsDateViewPids'] : '100';
$newsDateViewPids = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $commaSeparatedNewsDateViewPids);
$dateConf = array(
	array(
		'GETvar' => 'tx_news_pi1[controller]',
		'noMatch' => 'bypass',
	),
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][year]'
	),
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][month]',
	),
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][day]',
		'noMatch' => 'bypass',
	)
);
	// preserve already set fixedPostVars and add our page uids
if(is_array($TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'])){
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] += array_fill_keys($newsDateViewPids, $dateConf);
} else {
	$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'] = array_fill_keys($newsDateViewPids, $dateConf);
}

$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['page'] = array(
	array(
       	'GETvar' => 'tx_news_pi1[@widget_0][currentPage]',
	),
);

	// news rewrites if news list isn't on a predefined page.
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['c'] = array(
	array(
       	'GETvar' => 'tx_news_pi1[overwriteDemand][categories]',
	),
);

$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['t'] = array(
	array(
       	'GETvar' => 'tx_news_pi1[overwriteDemand][tags]',
	),
);


$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['d'] = array(
	array(
		'GETvar' => 'tx_news_pi1[controller]',
		'noMatch' => 'bypass',
	),
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][year]'
	),
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][month]',
	),
	array(
		'GETvar' => 'tx_news_pi1[overwriteDemand][day]',
		'noMatch' => 'bypass',
	)
);

/*
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars']['7'] = array(
	// it's a section for the page browser "blog/2/" like
	array(
		'GETvar' =>  'tx_news_pi1[@widget_0][currentPage]',
	),
);
*/



	// RSS
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fileName']['index']['rss.xml'] = array(
	'keyValues' => array(
		'type' => 9818,
	)
);

?>