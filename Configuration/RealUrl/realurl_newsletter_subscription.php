<?php

$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['newsletter'] = array(
	array(
       	'GETvar' => 'tx_pxanewslettersubscription_subscription[action]',
		'valueMap' => array(
			'confirmm' => 'ajax',
			'confirmp' => 'confirm',
		),
	),
);
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['action'] = array(
	array(
       	'GETvar' => 'status',
	),
);
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['id'] = array(
	array(
       	'GETvar' => 'hashid',
	),
);
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['postVarSets']['_DEFAULT']['hash'] = array(
	array(
       	'GETvar' => 'hash',
	),
);
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['fileName']['index']['subscription.html'] = array(
	'keyValues' => array(
		'type' => 6171239,
	),
);

?>