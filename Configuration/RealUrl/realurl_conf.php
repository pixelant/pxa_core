<?php
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['init'] = array(
    'enableCHashCache' => '1',
    'appendMissingSlash' => 'ifNotFile',
    'enableUrlDecodeCache' => '1',
    'enableUrlEncodeCache' => '1'
);
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT']['pagePath'] = array(
    'type' => 'user',
    'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
    'spaceCharacter' => '-',
    'languageGetVar' => 'L',
    'expireDays' => '90',
    'encodeTitle_userProc' => 'Pixelant\PxaCore\Hooks\URLEncode->encodeTitle'
);

include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_news.php';
include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_felogin.php';
include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_tq_seo.php';
include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_realurl_404_multilingual.php';
include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_news_blog.php';
include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_comments.php';
include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_core') . 'Configuration/RealUrl/realurl_newsletter_subscription.php';
?>
