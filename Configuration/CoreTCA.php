<?php

# Allow mimages wider than 999px and higher than 700px which is TYPO3 default
$TCA['tt_content']['columns']['imagewidth']['config']['range']['upper'] = 1280;
$TCA['tt_content']['columns']['imageheight']['config']['range']['upper'] = 1280;



$TCA['tx_flexslider_domain_model_flexslider']['columns']['link']['config']['wizards']['link']['params']['blindLinkFields'] = 'target,class,params,title';
$TCA['tx_flexslider_domain_model_flexslider']['columns']['link']['config']['wizards']['link']['params']['blindLinkOptions'] = 'file,folder';
// [blindLinkFields]=target,title,class,params
// [blindLinkOptions]=folder,file,mail,spec

# Fixes copying TAG:S
$TCA['tx_news_domain_model_tag']['columns']['title']['config']['eval'] = 'required,trim';


	// make .ts and .less editable
$GLOBALS['TYPO3_CONF_VARS']['SYS']['textfile_ext'] .= ',less,ts';


?>
