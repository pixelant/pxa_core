
# Add Class property to some of the elements f.ex. textline, buttons
mod.wizards.form.elements.textline.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.button.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.submit.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.reset.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.checkbox.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.fileupload.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.password.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.radio.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.select.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.textarea.accordions.attributes.showProperties := addToList(class)
mod.wizards.form.elements.checkboxgroup.accordions.various.showProperties := addToList(class)
mod.wizards.form.elements.radiogroup.accordions.various.showProperties := addToList(class)

// Add 
mod.wizards {
	newContentElement.wizardItems {
		forms.elements {
			mailform {
				tt_content_defValues {
					bodytext (
enctype = multipart/form-data
method = post
prefix = tx_form
10 = FIELDSET
10 {
	legend {
		value = Name of form
	}
}
					)
				}
			}
		}
	}
	form {
		defaults {
			tabs {
				elements {
					accordions {
						predefined {
							showButtons := removeFromList(name)
						}
					}
				}
			}
		}
	}
}

mod.wizards.newContentElement.wizardItems.plugins.elements.tx_gomapsext_show {
	icon = ../typo3conf/ext/pxa_core/Resources/Public/Extensions/cbgooglemaps/Icons/ce_wiz.gif
	title = LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:tt_content.newContentElement.tx_gomapsext_show.title
	description = LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:tt_content.newContentElement.tx_gomapsext_show.description
	tt_content_defValues {
		CType = list
		list_type = cbgooglemaps_quickgooglemap
	}
}

// Re enable image_frames on image content, which is disabled by css-styled-content
TCEFORM.tt_content.image_frames.disabled = 0

#############
#### RTE ####
#############
RTE {

    default {
        showButtons = formatblock, left, center, right, orderedlist, unorderedlist, bold, italic,   link,  undo, redo,  fontsize,    definitionlist, quotation, sample, small, span,  strong, variable, insertcharacter, textcolor, chMode, findreplace, insertparagraphbefore, insertparagraphafter, removeformat
        hideButtons = language, showlanguagemarks, formattext, bidioverride, big, citation, code, definition, deletedtext, definitionitem, emphasis, insertedtext, keyboard, monospaced, bgcolor, blockstylelabel, textindicator, editelement, showmicrodata, emoticon, insertsofthyphen, strikethrough,  blockstyle, line, unlink, user, acronym, spellcheck,   justifyfull, inserttag, copy, cut, paste, pastetoggle, pastebehaviour, about, toggleborders, table, tableproperties, tablerestyle, rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit, columnproperties, columninsertbefore, columninsertafter, columndelete, columnsplit, cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, subscript, superscript, lefttoright, righttoleft, outdent, indent, textstyle, fontstyle,  underline, textstylelabel,    cellmerge, textcolor, image


        toolbarOrder =


        enableWordClean = 1
        removeTrailingBR = 1
        removeComments = 1
        removeTags = center, font, o:p, sdfield, strike, u
        removeTagsAndContents = link, meta, script, style, title

        RTEHeightOverride = 450
        RTEWidthOverride = 650

        keepButtonGroupTogether = 1

        showStatusBar =  1


        buttons.formatblock.orderItems = h1, h2, h3, h4, h5, h6, p, blockquote
        buttons.left.useClass = text-left
        buttons.center.useClass = text-center
        buttons.right.useClass = text-right



        #contentCSS = typo3conf/ext/t3layout_ricky/Resources/Public/Css/rte.css
        useCSS = 0

        proc {
            dontConvBRtoParagraph = 1
            remapParagraphTag = p
            keepPDIVattribs = xml:lang,class,style,align
            dontRemoveUnknownTags_db = 1
            preserveTables = 0
        }
    }
}

// Add button styles to links (file, page, url, mail, spec)
#RTE.classesAnchor.btn.class = btn
#RTE.classesAnchor.btn.type = page

#RTE.classesAnchor.btn-info.class = btn btn-info
#RTE.classesAnchor.btn-info.type = page

#RTE.classes.btn.name = Button
#RTE.classes.btn-info.name = Button Info

#RTE.default {
	#contentCSS >
	#proc.allowedClasses := addToList(btn,btn-info)
	#classesLinks := addToList(btn,btn-info)
	#classesAnchor := addToList(btn,btn-info)
#}


# Configuration of TCEMAIN
TCEMAIN.table.tt_content {
	# Content will NOT have "(copy)" appended:
	disablePrependAtCopy = 1
}

TCEMAIN.permissions {
	# ID of the backend group that will own new pages
	groupid = 1	
	group = show, editcontent, edit, delete, new
}


# Configuration of previewFrameWidths
mod.web_view.previewFrameWidths { 	 
  320 {
  	label = LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:previewFrameWidths.320.label
  }
  768 {
  	label = LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:previewFrameWidths.768.label
  } 	
  1024 {
  	label = LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:previewFrameWidths.1024.label
  }
  1280 {
  	label = LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:previewFrameWidths.1280.label
  }
  300 >
  360 >	
  400 >
  480 >
  600 >
  640 >
  800 >
  960 >
}