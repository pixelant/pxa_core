# typoscript objects
#<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/menu.ts">
#<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/elements.ts">

# TYPO3
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/language.ts">

# Extensions
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/cbgooglemaps.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/pxa_solr.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/pxa_newsletter_subscription.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/tq_seo.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/aloha.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/flexslider.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/news.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/news_blog.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/comments.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/felogin.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Setup/Extensions/pxa_generic_content.ts">