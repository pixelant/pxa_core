
# customsubcategory=zm_news=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.news
# customsubcategory=zn_news_rss=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.rss
# customsubcategory=zo_blog_rss=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.blog_rss
# customsubcategory=aa_setup=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.setup
# customsubcategory=ga_tracking=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.tracking
# customsubcategory=felogin=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.felogin
# customsubcategory=zu_addThis=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.addThis
# customsubcategory=cc_enable=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.enable
# customsubcategory=bb_language=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.language


tx_pxacore.config {
	
	felogin {
		# cat=plugin.tx_pxacore/felogin/aa; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.felogin_email_from
		email_from = login@core.pixelant.se
		# cat=plugin.tx_pxacore/felogin/ab; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.felogin_email_fromname
		email_fromName = Pixelant Core Login
		# cat=plugin.tx_pxacore/felogin/ac; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.felogin_replyto
		replyTo = repoly-to@core.pixelant.se
	}

	# cat=plugin.tx_pxacore/aa_setup/aa; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.sitedomain
	siteDomain = core.pixelant.nu

	# cat=plugin.tx_pxacore/bb_language/al; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.language
	language = en
	
	# cat=plugin.tx_pxacore/bb_language/am; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.locale_all
	locale_all = en_GB.UTF-8

	# cat=plugin.tx_pxacore/bb_language/an; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.htmlTag_setParams
	htmlTag_setParams = lang="en"

	# cat=plugin.tx_pxacore/bb_language/ao; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.disqusLocale
	disqusLocale = en

	# cat=plugin.tx_pxacore/bb_language/ao; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.googlePlusLocale
	googlePlusLocale = en

	# cat=plugin.tx_pxacore/bb_language/ao; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.facebookLocale
	facebookLocale = en

	# cat=plugin.tx_pxacore/bb_language/ap; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.googleapi_language
	googleapi_language = en

	blog {
		rss {
            # cat=plugin.tx_pxacore/zo_blog_rss/aa; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_title
            title = Pixelant Core RSS Title

            # cat=plugin.tx_pxacore/zo_blog_rss/ab; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_description
			description = Pixelant Core RSS Description

			# cat=plugin.tx_pxacore/zo_blog_rss/ac; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_copyright
			copyright = Pixelant Core RSS Copyright

			# cat=plugin.tx_pxacore/zo_blog_rss/ad; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_category
			category = Pixelant Core RSS Category

			# cat=plugin.tx_pxacore/zo_blog_rss/ae; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_generator
			generator = Pixelant Core RSS Generator

			# cat=plugin.tx_pxacore/zo_blog_rss/ba; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_limit
            limit = 30
			
			# cat=plugin.tx_pxacore/zo_blog_rss/bb; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categories
			categories = 

			# cat=plugin.tx_pxacore/zo_blog_rss/bc; type=options[LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.0=or,LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.1=and,LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.2=notor,LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.3=notand]; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction
			categoryConjunction = notor

			# cat=plugin.tx_pxacore/zo_blog_rss/bd; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_startingpoint
            startingpoint = 95

			# cat=plugin.tx_pxacore/zo_blog_rss/be; type=string; label=RSS language
			language = sv_SE
		}
	}
	news {
		
		# cat=plugin.tx_pxacore/zm_news/aa; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_detailpid
        detailPid = 45

		# cat=plugin.tx_pxacore/zm_news/ab; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_storagepid
        storagePid = 38

		# cat=plugin.tx_pxacore/zm_news/ac; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_showallpid
        showAllPid = 7

        # cat=plugin.tx_pxacore/zm_news/ad; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_listpid
		listPid = 7

		rss {
            # cat=plugin.tx_pxacore/zn_news_rss/aa; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_title
            title = Pixelant Core RSS Title

            # cat=plugin.tx_pxacore/zn_news_rss/ab; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_description
			description = Pixelant Core RSS Description

			# cat=plugin.tx_pxacore/zn_news_rss/ac; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_copyright
			copyright = Pixelant Core RSS Copyright

			# cat=plugin.tx_pxacore/zn_news_rss/ad; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_category
			category = Pixelant Core RSS Category

			# cat=plugin.tx_pxacore/zn_news_rss/ae; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_generator
			generator = Pixelant Core RSS Generator

			# cat=plugin.tx_pxacore/zn_news_rss/ba; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_limit
            limit = 30
			
			# cat=plugin.tx_pxacore/zn_news_rss/bb; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categories
			categories = 

			# cat=plugin.tx_pxacore/zn_news_rss/bc; type=options[LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.0=or,LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.1=and,LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.2=notor,LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction.I.3=notand]; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_categoryconjunction
			categoryConjunction = notor

			# cat=plugin.tx_pxacore/zn_news_rss/bd; type=int; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.news_rss_startingpoint
            startingpoint = 38

			# cat=plugin.tx_pxacore/zn_news_rss/be; type=string;  label=RSS language
			language = sv_SE

		}
	}

	width {
	 	normal {
			span1 = 60
			span2 = 140
			span3 = 220
			span4 = 300
			span5 = 380
			span6 = 460
			span7 = 540
			span8 = 620
			span9 = 700
			span10 = 780
			span11 = 860
			span12 = 940
		}
		wide {
			span1 = 70
			span2 = 170
			span3 = 270
			span4 = 370
			span5 = 470
			span6 = 570
			span7 = 670
			span8 = 770
			span9 = 870
			span10 = 970
			span11 = 1070
			span12 = 1170	
		}
	}
	useWideLayout = 1

	tracking {

		# cat=plugin.tx_pxacore/ga_tracking/aa; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.ga_account
		ga_account = 

		# cat=plugin.tx_pxacore/ga_tracking/ac; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.tracking_url
		url = http://www.resultify.com/clients

		# cat=plugin.tx_pxacore/ga_tracking/ae; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.tracking_site
		site = core
	}

#	comments {
		# cat=plugin.tx_pxacore/comments/ca; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.disableCommetnsOnPages
#		disableCommetnsOnPages = 1
		# cat=plugin.tx_pxacore/comments/cb; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.disableCommetnsOnNewsDetail
#		disableCommetnsOnNewsDetail = 1
		# cat=plugin.tx_pxacore/comments/cc; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.moderateNewComments
#		moderateNewComments = 0
		# cat=plugin.tx_pxacore/comments/cd; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.secondsBetweenTwoComments
#		secondsBetweenTwoComments = 0
		# cat=plugin.tx_pxacore/comments/ce; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.showGravatarImage
#		showGravatarImage = 1
		# cat=plugin.tx_pxacore/comments/cd; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.countCommentsInNewsList
#		countCommentsInNewsList = 0

#		relativeDate.absoluteFormatFallback = %A %e, %Y at %I:%M %p
#		enableRepliesToComments = 1
#		showRepliesToComments = 1
#		countReplies = 0
		
#	}

## Not included in t3layout
#	addThis {
		# cat=plugin.tx_pxacore/zu_addThis/aa; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.enabled
#		enabled = 0
		# cat=plugin.tx_pxacore/zu_addThis/ab; type=string; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.pubid
#		pubid = 
#		class = addthis_toolbox addthis_default_style addthis_16x16_style pull-right
#		style = padding-top:5px; padding-bottom: 5px;
#		buttons {
			# cat=plugin.tx_pxacore/zu_addThis/ac; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.buttons.facebook
#			facebook = 1
			# cat=plugin.tx_pxacore/zu_addThis/ad; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.buttons.twitter
#			twitter = 1
			# cat=plugin.tx_pxacore/zu_addThis/ae; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.buttons.google_plusone_share
#			google_plusone_share = 1
			# cat=plugin.tx_pxacore/zu_addThis/af; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.buttons.linkedin
#			linkedin = 0
			# cat=plugin.tx_pxacore/zu_addThis/ag; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.buttons.pinterest_share
#			pinterest_share = 0
			# cat=plugin.tx_pxacore/zu_addThis/ah; type=boolean; label=LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.addThis.buttons.compact
#			compact = 1
#		}
#	}

} 




styles.content.imgtext.linkWrap.lightboxEnabled = 1
styles.content.imgtext.linkWrap.lightboxRelAttribute = lightbox

# Extensions
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Constants/Extensions/news.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Constants/Extensions/flexslider.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:pxa_core/Configuration/TypoScript/Includes/Constants/Extensions/aloha.ts">
