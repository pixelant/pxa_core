plugin.tx_flexslider {
	settings {
		aloha {
			text {
				tag=span
				allow=up,down,move,edit,hide,newContentElementBelow,delete
				class=nostyles alohaeditable-block
			}
			input {
				allow = edit
				class=nostyles
			}
		}

		keyboardNav = 0
		subtitle {
			maxCharacters = 135
		}
	}
}

plugin.tx_flexslider.view {
	layoutRootPath >
	layoutRootPaths {
		100 = EXT:pxa_core/Resources/Private/Extensions/flexslider/Layouts
		999 = {$plugin.tx_flexslider.view.layoutRootPath}
	}
	templateRootPath >
	templateRootPaths {
		100 = EXT:pxa_core/Resources/Private/Extensions/flexslider/Templates
		999 = {$plugin.tx_flexslider.view.templateRootPath}
	}
	partialRootPath >
	partialRootPaths {
		100 = EXT:pxa_core/Resources/Private/Extensions/flexslider/Partials
		999 = {$plugin.tx_flexslider.view.partialRootPath}
	}
}
