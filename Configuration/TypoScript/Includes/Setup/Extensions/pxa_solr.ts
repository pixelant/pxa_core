plugin.tx_pxasolr._LOCAL_LANG.default.pages = Pages
plugin.tx_pxasolr._LOCAL_LANG.sv.pages = Sidor
plugin.tx_pxasolr.settings.specialTags.pages = pages
plugin.tx_pxasolr.settings.specialTags.pages.returnFields = *, score

plugin.tx_pxasolr._LOCAL_LANG.default.tx_news_domain_model_news = News
plugin.tx_pxasolr._LOCAL_LANG.sv.tx_news_domain_model_news = Nyheter
plugin.tx_pxasolr.settings.specialTags.tx_news_domain_model_news = news
plugin.tx_pxasolr.settings.specialTags.tx_news_domain_model_news.returnFields = *, score
plugin.tx_pxasolr.settings.newsSinglePid = {$tx_pxacore.config.news.detailPid}

plugin.tx_pxasolr._LOCAL_LANG.default.readmore = Läs mer...
plugin.tx_pxasolr._LOCAL_LANG.sv.readmore = Läs mer...
plugin.tx_pxasolr._LOCAL_LANG.en.readmore = Read more...


plugin.tx_pxasolr._LOCAL_LANG.sv {
	javascriptRequired = Denna sökfunktion kräver javascript.
	search = Sök
	searchTitle = Sök
	all = Alla resultat
	seeMore = Se flera...
	resultsA = 
	resultsB = av
	resultsC = resultat
	empty = Inga resultat hittades.
}



plugin.tx_solr.index.queue {

	tx_news_domain_model_news = 1
	tx_news_domain_model_news {
		table = tx_news_domain_model_news

		fields {
			abstract = teaser

			author = author
			authorEmail_stringS = author_email

			title = title

			created = datetime
			
			content = SOLR_CONTENT
			content {
				field = bodytext
			}
			
			/*
			content = SOLR_CONTENT
			content {
				cObject = COA
				cObject {
					10 = TEXT
					10 {
						field = bodytext
						noTrimWrap = || |
					}
					
					20 = SOLR_RELATION
					20 {
						localField = content_elements
						foreignLabelField = bodytext
						singleValueGlue = | |
					}
					
				}
			}
			*/

			category_stringM = SOLR_RELATION
			category_stringM {
				localField = categories
				multiValue = 1
			}

			keywords = SOLR_MULTIVALUE
			keywords {
				field = keywords
			}

			tags_stringM = SOLR_RELATION
			tags_stringM {
				localField = tags
				multiValue = 1
			}

			url = TEXT
			url {
				typolink.parameter = {$tx_pxacore.config.news.detailPid}
				typolink.additionalParams = &tx_news_pi1[controller]=News&tx_news_pi1[action]=detail&tx_news_pi1[news]={field:uid}
				typolink.additionalParams.insertData = 1
				typolink.useCacheHash = 1
				typolink.returnLast = url
			}
		}

		attachments {
			fields = related_files
		}
	}

}

#plugin.tx_solr.logging.indexing.queue.tx_news_domain_model_news = 1


# Empty stuff and set template paths
page.includeCSS.pxasolr >

plugin.tx_pxasolr {
	view {
		templateRootPaths {
			default = {$plugin.tx_pxasolr.view.templateRootPath}
			10 = typo3conf/ext/pxa_core/Resources/Private/Extensions/tx_pxasolr/Templates
		}
		layoutRootPaths {
			default = {$plugin.tx_pxasolr.view.layoutRootPath}
			10 = typo3conf/ext/pxa_core/Resources/Private/Extensions/tx_pxasolr/Layouts
		} 
		partialRootPaths {
			default = {$plugin.tx_pxasolr.view.partialRootPath}
			10 = typo3conf/ext/pxa_core/Resources/Private/Extensions/tx_pxasolr/Partials
		}
	}
	settings {
		
		suggestResults.enable = 1

		elements {
			prev = TEXT
			prev.value = <i class="icon-double-angle-left"></i>
			
			next = TEXT
			next.value = <i class="icon-double-angle-right"></i>
			
			loader = TEXT
			loader.value = <i class="icon-spinner icon-spin"></i>
		}

		quicksearch {
			form {
				class = searching
			}
			input {
				class = quicksearch
			}
			submit {
				enabled = 0
			}
		}
		searchbox {
			form {
				class = form-inline
			}
			input {
				class = form-control
			}
			submit {
				class = btn btn-primary
			}
		}
		searchresult {
			container {
				class = 
			}
			searchtagmenu {
				class = nav nav-tabs
				item {
					count {
						prefix = (
						suffix = )
						class = 
					}
				}
			}
			pagination {
				class = pagination
			}
			info {
				class = 
			}
		}

	}

		# Use typeahead instead
	jqueryUi = 0
}

#plugin.tx_pxasolr._CSS_DEFAULT_STYLE > 