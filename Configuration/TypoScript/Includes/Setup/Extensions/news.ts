plugin.tx_news {
	
	persistence {
		storagePid = {$tx_pxacore.config.news.storagePid}
	}

	settings {
		countCommentsInNewsList = {$tx_pxacore.config.comments.countCommentsInNewsList}
        displayDummyIfNoMedia = 0
		#detailPid = {$tx_pxacore.config.news.detailPid}
		#list.media.dummyImage = EXT:pxa_bootstrap/Resources/Public/img/login_background_circle.png
		defaultDetailPid = {$tx_pxacore.config.news.detailPid}
		showAllPid = {$tx_pxacore.config.news.showAllPid}
		listPid = {$tx_pxacore.config.news.listPid}
		detail {
			media {
				image {
					maxWidth = 870
					maxHeight = 420
				}
				video {
					maxWidth = 870
					maxHeight = 420
					width = 870
					height = 420
				}	
			}
			related {
				media {
					image {
						width = 270c
						height = 144c
					}
					video {
						width = 270
      					height = 144
					}
				}
			}
			showSocialShareButtons = 0
			facebook {
				enableComments = 0
				appId=
				admin=
			}
			disqus {
				enableComments = 0
				shortName=
			}
		}
		list {
			rss.channel {
				title = {$tx_pxacore.config.news.rss.title}
				description = {$tx_pxacore.config.news.rss.description}
				link = http://{$tx_pxacore.config.siteDomain}/
				language = {$tx_pxacore.config.news.rss.language}
				copyright = {$tx_pxacore.config.news.rss.copyright}
				category = {$tx_pxacore.config.news.rss.category}
				generator = {$tx_pxacore.config.news.rss.generator}
				generator_image_url = {$tx_pxacore.config.filePath.images}logo.png
				generator_image_width = 226
				generator_image_height = 50
			}
			paginate {
				insertAbove = 0
			}
		}
		# Added settings so we can define different meda formats per "templateLayout"
		templateLayouts {
      		slidevertical {
      			media.image.width = 220c
      			media.image.height = 134c
      			media.video.width = 220
      			media.video.height = 134
      		}
      		articlelist {
      			media.image.width = 870c
      			media.image.height = 420c
      			media.video.width = 870
      			media.video.height = 420
      		}
    	}

    	interfaces {
			media {
				# Remove original youtube
				video := removeFromList(Tx_News_MediaRenderer_Video_Youtube)
				# Remove videosites, so it will be last (addin it again)
				video := removeFromList(Tx_News_MediaRenderer_Video_Videosites)
				# Add custom youtube
				video := addToList(Tx_PxaBootstrap_MediaRenderer_Video_Youtube)
				# Remove videosites, so it will be last (addin it again)
				video := addToList(Tx_News_MediaRenderer_Video_Videosites)
				#Tx_News_MediaRenderer_Audio_Mp3,Tx_News_MediaRenderer_Video_Quicktime,Tx_News_MediaRenderer_Video_File,Tx_News_MediaRenderer_Video_Youtube,Tx_News_MediaRenderer_Video_Videosites
			}
		}
	}

	_LOCAL_LANG {
		default {
			showall_link = Show all News
			label.categories = Categories:
			label.category = Category:
			categorylist.separator = ,
			label.datetime = Published:
			label.tstamp = Updated:
			label.fbShareText = Share
		}
		sv {
			showall_link = Visa alla nyheter
			label.categories = Kategorier:
			label.category = Kategori:
			categorylist.separator = ,
			label.datetime = Publicerad:
			label.tstamp = Uppdaterad:
			label.fbShareText = Dela
		}
	}
}


[globalVar = TSFE:type = 9818]
config {
    disableAllHeaderCode = 1
    xhtml_cleaning = none
    admPanel = 0
    metaCharset = utf-8
    additionalHeaders = Content-Type:text/xml;charset=utf-8
    disablePrefixComment = 1
    baseURL = http://{$tx_pxacore.config.siteDomain}/
    absRefPrefix = http://{$tx_pxacore.config.siteDomain}/
}

pageNewsRSS = PAGE
pageNewsRSS {
    typeNum = 9818
    10 < tt_content.list.20.news_pi1
    10 {
            switchableControllerActions {
                    News {
                            1 = list
                    }
            }
            settings < plugin.tx_news.settings
            settings {
                    categories = {$tx_pxacore.config.news.rss.categories}
                    categoryConjunction = {$tx_pxacore.config.news.rss.categoryConjunction}
                    limit = {$tx_pxacore.config.news.rss.limit}
                    detailPid = {$tx_pxacore.config.news.detailPid}
                    startingpoint = {$tx_pxacore.config.news.rss.startingpoint}
                    format = xml
            }
    }
}
[global]


plugin.tx_news {
  _LOCAL_LANG.sv {
    search-text = Det finns %1$ resultat för "%2$".
    search-start = Sök
    search-noresult = Inga nyheter hittades.
    back-link = Tillbaka
    more-link = Läs mer
    related-news = Relaterade nyheter
    related-files = Relaterade filer
    related-links = Relaterade länkar
    author = skapad av %s
    paginate_overall = Sida %s till %s.
    paginate_next = Nästa
    paginate_previous = Föregående
    entry = post
    entries = poster
    month.01 = Januari
    month.02 = Februari
    month.03 = Mars
    month.04 = April
    month.05 = Maj
    month.06 = Juni
    month.07 = Juli
    month.08 = Augusti
    month.09 = September
    month.10 = Oktober
    month.11 = November
    month.12 = December
    list_nonewsfound = Inga nyheter tillgängliga.
  }
}

[PIDinRootline = 97,102]
plugin.tx_news {
  _LOCAL_LANG.sv {
    list_nonewsfound = Inga bloggar tillgängliga.
  }
}
[global]