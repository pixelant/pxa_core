config.tx_aloha {
	topBar {
		warningMessage.disable = 1
	}
}

tt_content.image.20.rendering.singleNoCaption.imgTagStdWrap.editIcons >
tt_content.image.20.rendering.singleNoCaption.imgTagStdWrap.alohaPostProcess = 1
tt_content.image.20.rendering.singleNoCaption.imgTagStdWrap.alohaPostProcess {
	class = nostyles alohaeditable-inline-block
	field = image
	allow = edit
}

# Change output of headers when aloha is enaled
[globalVar = TSFE : beUserLogin > 0] && [userFunc = isAlohaEnabledForUser]

# This CASE cObject renders the header content:
  # currentValue is set to the header data, possibly wrapped in link-tags.
lib.stdheader {

  10 = CASE
  10.setCurrent {
    field = header
    htmlSpecialChars = 1
    typolink.parameter.field = header_link
  }
  10.key.field = header_layout
  10.key.ifEmpty = {$content.defaultHeaderType}
  10.key.ifEmpty.override.data = register: defaultHeaderType

  10.1 = TEXT
  10.1.10 >
  10.1.20 >
  10.1.current = 1
  10.1.stdWrap.dataWrap = <h1{register:headerClass}>|</h1>

  10.default < .10.1

  10.2 < .10.1
  10.2.stdWrap.dataWrap = <h2{register:headerClass}>|</h2>

  10.3 < .10.1
  10.3.stdWrap.dataWrap = <h3{register:headerClass}>|</h3>

  10.4 < .10.1
  10.4.stdWrap.dataWrap = <h4{register:headerClass}>|</h4>

  10.5 < .10.1
  10.5.stdWrap.dataWrap = <h5{register:headerClass}>|</h5>

  # HTML5 subheader
  20 >
  20 = CASE
  20 {
    key {
      field = header_layout
    }

    default = TEXT
    default {
      wrap = <h1><small>|</small></h1>
      htmlSpecialChars = 1
      field = subheader
    }

    1 < .default

    2 < .default
    2.wrap = <h3><small>|</small></h3>

    3 < .default
    3.wrap = <h4><small>|</small></h4>

    4 < .default
    4.wrap = <h5><small>|</small></h5>

    5 < .default
    5.wrap = <h6><small>|</small></h6>

    if {
      isTrue {
        field = subheader
      }
      value = html5
      equals.data = TSFE:config|config|doctype
      # Hide subheader for old style form element (it is used for recipient mail)
      isFalse = 0
      isFalse.override = 1
      isFalse.override {
        if.equals.field = CType
        if.value = mailform
      }
    }
  }
}

[global]
