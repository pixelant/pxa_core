# Styling
plugin.tx_pwcomments._CSS_DEFAULT_STYLE >

plugin.tx_pwcomments._CSS_DEFAULT_STYLE  (
  .hide_initally {
    display: none;
  }
)

# Translations
plugin.tx_pwcomments {
  _LOCAL_LANG.default {
    tx_pwcomments.newComment.mail = E-mail:
    tx_pwcomments.newComment.comment = Comment:
    tx_pwcomments.newComment.submit = Post comment
    tx_pwcomments.newComment.name = Name:
    tx_pwcomments.writeNewCommentReply = Leave a reply
    tx_pwcomments.writeNewComment = Leave a comment
  }
  _LOCAL_LANG.sv {
    tx_pwcomments.newComment.mail = E-post:
    tx_pwcomments.newComment.comment = Kommentar:
    tx_pwcomments.newComment.submit = Skicka
    tx_pwcomments.newComment.name = Namn:
    tx_pwcomments.writeNewCommentReply = Lämna ett svar
    tx_pwcomments.writeNewComment = Lämna en kommentar
    tx_pwcomments.reply=Svara
    tx_pwcomments.noComments=Inga kommentarer!
    tx_pwcomments.thanks=Tack för er kommentar
    tx_pwcomments.moderationNotice=Er kommentar kommer att kontrolleras innan den publiceras. Tack för ert tålamod!
    tx_pwcomments.relativeDate.wrap=%s sedan
    #tx_pwcomments.relativeDate.wrapAbsolute=at %s
    tx_pwcomments.relativeDate.fewSeconds=ett per sekunder
    tx_pwcomments.relativeDate.minute=minut
    tx_pwcomments.relativeDate.hour=timme
    tx_pwcomments.relativeDate.day=dag
    tx_pwcomments.relativeDate.week=vecka
    tx_pwcomments.relativeDate.pluralSuffix=
    tx_pwcomments.relativeDate.pluralSuffixForDay=
  }
}

##############################################
config.tx_extbase.persistence.classes {
   Tx_PwComments_Domain_Model_FrontendUser {
       mapping {
           tableName = fe_users
       }
   }
}

# Settings
plugin.tx_pwcomments {
  settings {
    image {
      maxHeight = 40
      maxWidth = 40
    }
    showGravatarImage = {$tx_pxacore.config.comments.showGravatarImage}
    moderateNewComments = {$tx_pxacore.config.comments.moderateNewComments}
    secondsBetweenTwoComments = {$tx_pxacore.config.comments.secondsBetweenTwoComments}
  }
}
[globalVar = GP:tx_news_pi1|news > 0]
  # Enable the usage of entryUid and define entryUid
  plugin.tx_pwcomments.settings {
    useEntryUid = 1
    entryUid = TEXT
    entryUid.data = GP:tx_news_pi1|news
  }
 
[global]


tmp.comments = COA
  tmp.comments {
   # List comments
   20 < lib.pwCommentsIndex
   # Write new comment
   30 < lib.pwCommentsNew
}
page.10.variables.comments < tmp.comments


[globalVar = LIT:1 = {$tx_pxacore.config.comments.disableCommetnsOnPages}]
page.10.variables.comments >
[globalVar = LIT:1 = {$tx_pxacore.config.comments.disableCommetnsOnNewsDetail}] && [globalVar = GP:tx_news_pi1|news > 0]
page.10.variables.comments >
[globalVar = LIT:1 != {$tx_pxacore.config.comments.disableCommetnsOnNewsDetail}] && [globalVar = GP:tx_news_pi1|news > 0] && [globalVar = LIT:1 = {$tx_pxacore.config.comments.disableCommetnsOnPages}]
page.10.variables.comments < tmp.comments
[globalVar = TSFE:page|tx_realurl_pathsegment = 404]
page.10.variables.comments >
[global]