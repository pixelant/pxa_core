
[globalVar = TSFE:type = 9918]
config {
    disableAllHeaderCode = 1
    xhtml_cleaning = none
    admPanel = 0
    metaCharset = utf-8
    additionalHeaders = Content-Type:text/xml;charset=utf-8
    disablePrefixComment = 1
    baseURL = http://{$tx_pxacore.config.siteDomain}/
    absRefPrefix = http://{$tx_pxacore.config.siteDomain}/
}

pageNewsRSS = PAGE
pageNewsRSS {
    typeNum = 9918
    10 < tt_content.list.20.news_pi1
    10 {
            switchableControllerActions {
                    News {
                            1 = list
                    }
            }
            settings < plugin.tx_news.settings
            settings {
                    categories = {$tx_pxacore.config.blog.rss.categories}
                    categoryConjunction = {$tx_pxacore.config.blog.rss.categoryConjunction}
                    limit = {$tx_pxacore.config.blog.rss.limit}
                    detailPid = {$tx_pxacore.config.blog.detailPid}
                    startingpoint = {$tx_pxacore.config.blog.rss.startingpoint}
                    format = xml
            }
    }
}
[global]