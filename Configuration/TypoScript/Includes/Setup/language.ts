# DEFAULT 
config {
	// Language Settings
	uniqueLinkVars = 1
	linkVars := addToList(L)
	sys_language_uid = 0
	sys_language_overlay = hideNonTranslated
	sys_language_mode = strict
	
	language = {$tx_pxacore.config.language}
	locale_all = {$tx_pxacore.config.locale_all}
	htmlTag_setParams = {$tx_pxacore.config.htmlTag_setParams}
	
}

plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language={$tx_pxacore.config.googleapi_language}
plugin.tx_news.settings.disqusLocale = {$tx_pxacore.config.disqusLocale}
plugin.tx_news.settings.facebookLocale = {$tx_pxacore.config.facebookLocale}
plugin.tx_news.settings.googlePlusLocale = {$tx_pxacore.config.googlePlusLocale}
#plugin.tx_pxasolr.settings.formats.date = d.m.Y

# SWEDISH
[globalVar = GP:L = 1]

	config {
	 	sys_language_uid = 1
		language = sv
		locale_all = sv_SE.UTF-8
		htmlTag_setParams = lang="sv"
	}

	plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language=sv-se
	plugin.tx_solr.solr.path = /solr/core_se/
	plugin.tx_news.settings.disqusLocale = sv_SE
	plugin.tx_news.settings.facebookLocale = sv_SE
	plugin.tx_news.settings.googlePlusLocale = sv_SE
	plugin.tx_pxasolr.settings.formats.date = Y-m-d

[global]

# ENGLISH
[globalVar = GP:L = 2]

	config {
	 	sys_language_uid = 2
		language = en
		locale_all = en_GB.UTF-8
		htmlTag_setParams = lang="en"
	}

	plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language=en
	plugin.tx_solr.solr.path = /solr/core_en/
	plugin.tx_news.settings.disqusLocale = en
	plugin.tx_news.settings.facebookLocale = en
	plugin.tx_news.settings.googlePlusLocale = en
	plugin.tx_pxasolr.settings.formats.date = d.m.Y
	
[global]

# DANISH
[globalVar = GP:L = 3]

	config {
		sys_language_uid = 3
		language = da
		locale_all = da_DK.utf8
		htmlTag_setParams = lang="da"
	}

	plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language=da
	plugin.tx_solr.solr.path = /solr/core_da/
	plugin.tx_news.settings.disqusLocale = da
	plugin.tx_news.settings.facebookLocale = da
	plugin.tx_news.settings.googlePlusLocale = da

[global]

# FINNISH
[globalVar = GP:L = 4]

	config {
		sys_language_uid = 4
		language = fi
		locale_all = fi_FI.utf8
		htmlTag_setParams = lang="fi"
	}

	plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language=fi
	plugin.tx_solr.solr.path = /solr/core_fi/
	plugin.tx_news.settings.disqusLocale = fi
	plugin.tx_news.settings.facebookLocale = fi
	plugin.tx_news.settings.googlePlusLocale = fi

[global]

# GERMAN
[globalVar = GP:L = 5]

	config {
		sys_language_uid = 5
		language = de
		locale_all = de_DE.utf8
		htmlTag_setParams = lang="de"	
	}

	plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language=de
	plugin.tx_solr.solr.path = /solr/core_de/
	plugin.tx_news.settings.disqusLocale = de
	plugin.tx_news.settings.facebookLocale = de
	plugin.tx_news.settings.googlePlusLocale = de

[global]

# NORWEGIAN
[globalVar = GP:L = 6]

	config {
		sys_language_uid = 6
		language = no
		locale_all = no_NO.utf8
		htmlTag_setParams = lang="no"
	}

	plugin.tx_cbgooglemaps.settings.googleapi.uri = http://maps.google.com/maps/api/js?sensor=false&amp;language=no
	plugin.tx_solr.solr.path = /solr/core_no/
	plugin.tx_news.settings.disqusLocale = no
	plugin.tx_news.settings.facebookLocale = no
	plugin.tx_news.settings.googlePlusLocale = no

[global]
