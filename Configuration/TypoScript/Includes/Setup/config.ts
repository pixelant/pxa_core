config {
	// Administrator settings
	#admPanel = {$config.adminPanel}
	#debug = {$config.debug}

	// Set baseURL from constants
	baseURL = http://{$tx_pxacore.config.siteDomain}/
	absRefPrefix = /
	
	doctype = html5
	htmlTag_setParams = none

	// Character sets
	renderCharset = utf-8
	metaCharset = utf-8

	// Cache settings
	cache_period = 43200
	sendCacheHeaders = 1

	// URL Settings
	tx_realurl_enable = 1
	simulateStaticDocuments = 0

	// Link settings
	prefixLocalAnchors = all

	// Remove targets from links
	intTarget =
	extTarget =

	// Indexed Search
	index_enable = 0
	#index_externals = 1

	// Code cleaning
	disablePrefixComment = 1

	// Move default CSS and JS to external file
	removeDefaultJS = external
	inlineStyle2TempFile = 1

	// Protect mail addresses from spamming
	spamProtectEmailAddresses = -3
	spamProtectEmailAddresses_atSubst = @<span style="display:none;">remove-this.</span>

	// Comment in the <head> tag
	headerComment (
				This website is brought to you by Pixelant. http://www.pixelant.se/

	)

	aloha = 1

	concatenateCss = 1

	concatenateJs = 1

	linkVars = 
	
	tx_pxacore {
		responsiveView {
			buttons {
				desktop.disable = 0
				laptop.disable = 0
				tablet.disable = 0
				mobile {
					disable = 0
					// You can use this to set width interval for given view
					#minWidth = 340px
					#maxWidth = 340px
				}
			}
		}
	}
}

# Check if aloha is enabled, need to make some changes in ts, like it is in aloha because it has been overwritten
[globalVar = TSFE : beUserLogin > 0] && [userFunc = isAlohaEnabledForUser]

	# Disable realurl we don't break links in bodytext
	# TODO: see if we can disable realurl only for rtehtmlarea when aloha edit is enabled
	config.tx_realurl_enable = 0

	# Make sure no_cache is set
	config.no_cache = 1

	# Disable spamProtectEmailAddresses so we don't break email links
	config.spamProtectEmailAddresses = 0

[global]


[browser = msie]
    # How to Use X-UA-Compatible
    # The best practice is an X-UA-Compatible HTTP Header. 
    # Adding the directive to the response header tells Internet Explorer what engine to use before parsing content begins. 
  config.additionalHeaders = X-UA-Compatible: IE=edge

[global]