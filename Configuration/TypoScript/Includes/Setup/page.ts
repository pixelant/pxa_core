##### Make the PAGE object #####
page = PAGE
page {
	# Regular pages always have typeNum = 0
	typeNum = 0

	# Add the icon that will appear in front of the url in the browser
	# This icon will also be used for the bookmark menu in browsers
	## shortcutIcon = {$tx_pxacore.config.filePath.images}favicon.ico

}


# Google Analytics track pageview and add script for event tracking
[globalString = LIT:{$tx_pxacore.config.tracking.ga_account} = /(UA)-[\d-]+/]
page.headerData.17890 = TEXT
page.headerData.17890.value (
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '{$tx_pxacore.config.tracking.ga_account}', '{$tx_pxacore.config.siteDomain}');
ga('send', 'pageview');
</script>
)
page.includeJSFooter {
  tracking = {$tx_pxacore.config.tracking.url}/{$tx_pxacore.config.tracking.site}
  tracking.external = 1
}
[global]
