<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

// Add Pixelant Core static to include in template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Pixelant Core');

if (TYPO3_MODE == 'BE' || TYPO3_MODE == 'FE' && isset($GLOBALS['BE_USER'])) {
	// minor styling changes
	$GLOBALS['TBE_STYLES']['stylesheet'] = '../' .\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('pxa_core') . 'Resources/Public/Css/Backend/pxa_core.css';
}

// Add urltype tel:
// TODO make TYPO3 aware of it...
$TCA['pages']['columns']['urltype']['config']['items'][100] = array();
$TCA['pages']['columns']['urltype']['config']['items'][100][] = 'tel:';
$TCA['pages']['columns']['urltype']['config']['items'][100][] = 100;

// Add urltype tel to alternative page language:
// TODO make TYPO3 aware of it...
$TCA['pages_language_overlay']['columns']['urltype']['config']['items'][100] = array();
$TCA['pages_language_overlay']['columns']['urltype']['config']['items'][100][] = 'tel:';
$TCA['pages_language_overlay']['columns']['urltype']['config']['items'][100][] = 100;

// Limit images to one TODO: remove now multiple images fixed in pxa_bootstrap ts now ? 
// $TCA['tt_content']['columns']['image']['config']['maxitems'] = '1';

// Change palette, what fields in content that are shown when tt_content is fluidcontent.
// TODO: Update, should not hide some of the fields, f.ex. fluidcontent childs.
/*
$GLOBALS['TCA']['tt_content']['types']['fluidcontent_content']['showitem'] = '
	--div--;LLL:EXT:fluidcontent/Resources/Private/Language/locallang.xml:pages.tab.content_settings,
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.general;general,
	tx_fed_fcefile;LLL:EXT:fluidcontent/Resources/Private/Language/locallang.xml:pages.tab.element_type,
	pi_flexform;LLL:EXT:fluidcontent/Resources/Private/Language/locallang.xml:pages.tab.configuration,
	--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.appearance,
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.frames;frames,
	--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility,
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,
	--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,
	--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.extended;extended
	 ';
*/

// We don't want to see [ INVALID VALUE ("18181") ]
$TCA['tt_content']['columns']['colPos']['displayCond'] = 'FIELD:colPos:!=:18181';

// To style login
$TBE_STYLES['htmlTemplates']['EXT:backend/Resources/Private/Templates/login.html'] = 'EXT:pxa_core/Resources/Private/Templates/Backend/Login/login.html';

$requestUrl = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL');
$domain = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL') . TYPO3_mainDir;

$loginPageCheck = str_replace($domain, '', $requestUrl);
$_EXTCONF = unserialize($_EXTCONF);

if (TYPO3_MODE == 'BE') {
	if ($loginPageCheck == '' || $loginPageCheck == 'index.php' || substr($loginPageCheck, 0, 10) == 'index.php?') {
		$GLOBALS['TBE_STYLES']['styleSheetFile_post'] = '../' .\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('pxa_core') . 'Resources/Public/Css/Backend/pxa_core_be_login.css';
	}
}

# Add Google+ profileid to be_user
t3lib_div::loadTCA('be_users');
$TCA['be_users']['columns']['google_plus_profile_id'] = array(
    'exclude' => 0,
    'label' => 'LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:be_users.google_plus_profile_id',
    'config' => array(
    	'type'=>'input',
    	'eval'=>'trim',
		'max'=>'40',
		'size'=>'20',
    )
);
t3lib_extMgm::addToAllTCAtypes('be_users','google_plus_profile_id;;;;1-1-1',"","after:email");



## JUST A TEST TO SEE IF IT WAS EASY TO ADD A CONTENT TYPE  TESTED WITH A Definition list ##
/*
t3lib_div::loadTCA('tt_content');
$TCA['tt_content']['columns']['CType']['config']['items'][] = array('Definition list', 'definitionlist');
$TCA['tt_content']['types']['definitionlist'] = array(
    'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.general;general,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.header;header,
					bodytext;LLL:EXT:cms/locallang_ttc.xml:bodytext.ALT.bulletlist_formlabel;;nowrap,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.appearance,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.frames;frames,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.textlayout;textlayout,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility,
					--palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,
				--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended'
);
*/
?>