<?php
namespace Pixelant\PxaCore\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Pixelant, Pixelant
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 * @package TYPO3
 * @subpackage pxa_core
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class LoginController extends \TYPO3\CMS\Backend\Controller\LoginController {
	/**
	 * Make login news modified for Pixelant News
	 *
	 * @return string HTML content
	 */
	public function makeLoginNews() {
		$newsContent = '';
		$systemNews = $this->getSystemNews();
		// Traverse news array IF there are records in it:
		if (is_array($systemNews) && count($systemNews) && !\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('loginRefresh')) {
			/** @var $htmlParser \TYPO3\CMS\Core\Html\RteHtmlParser */
			$htmlParser = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Html\\RteHtmlParser');
			// Get the main news template, and replace the subpart after looped through
			$newsContent = \TYPO3\CMS\Core\Html\HtmlParser::getSubpart($GLOBALS['TBE_TEMPLATE']->moduleTemplate, '###LOGIN_NEWS###');
			$newsItemTemplate = \TYPO3\CMS\Core\Html\HtmlParser::getSubpart($newsContent, '###NEWS_ITEM###');
			$newsItem = '';
			$count = 1;
			foreach ($systemNews as $newsItemData) {
				$additionalClass = '';
				if ($count == 1) {
					$additionalClass = ' first-item';
				} elseif ($count == count($systemNews)) {
					$additionalClass = ' last-item';
				}
				$newsItemContent = $htmlParser->TS_transform_rte($htmlParser->TS_links_rte($newsItemData['content']));
				$newsItemMarker = array(
					'###HEADER###' => htmlspecialchars($newsItemData['header']),
					'###DATE###' => htmlspecialchars($newsItemData['date']),
					'###LINK###' => $newsItemData['link'],
					'###CONTENT###' => $newsItemContent,
					'###CLASS###' => $additionalClass
				);
				$count++;
				$newsItem .= \TYPO3\CMS\Core\Html\HtmlParser::substituteMarkerArray($newsItemTemplate, $newsItemMarker);
			}
			$title = $GLOBALS['TYPO3_CONF_VARS']['BE']['loginNewsTitle'] ? $GLOBALS['TYPO3_CONF_VARS']['BE']['loginNewsTitle'] : $GLOBALS['LANG']->getLL('newsheadline');
			$newsContent = \TYPO3\CMS\Core\Html\HtmlParser::substituteMarker($newsContent, '###NEWS_HEADLINE###', htmlspecialchars($title));
			$newsContent = \TYPO3\CMS\Core\Html\HtmlParser::substituteSubpart($newsContent, '###NEWS_ITEM###', $newsItem);
		}
		return $newsContent;
	}

	/**
	 * Gets news from sys_news and converts them into a format suitable for
	 * showing them at the login screen.
	 *
	 * @return array An array of login news.
	 */
	protected function getSystemNews() {
    $extConf = unserialize(($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['pxa_core']));
		$GLOBALS['TYPO3_CONF_VARS']['BE']['loginNewsTitle'] = 'Pixelant ' . $GLOBALS['LANG']->sL('LLL:EXT:pxa_core/Resources/Private/Language/locallang_db.xlf:constants.label.category.news');
		$loginNews = array();
		try {
			$loginNews = $this->getFeed(($extConf['newsURL'] ? $extConf['newsURL'] : 'http://pixelant.se/news.xml'));
			$loginNews = $this->processRss($loginNews);
		} catch (HTTP_Request2_Exception $e) {
		}
		return $loginNews;
	}

	protected function processRss($rssArray, $itemLimit = 3, $descLimit = 300) {
		$loginNews = array();
		foreach ($rssArray['channel']['item'] as $key => $item) {
			if ($key >= $itemLimit)
				break;
			$loginNews[] = array(
				'header' => $item['title'],
				'date' => date($GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'], strtotime($item['pubDate'])),
				'link' => $item['link'],
				'content' => \TYPO3\CMS\Core\Utility\GeneralUtility::fixed_lgd_cs($item['description'], $descLimit)
			);
		}
		return $loginNews;
	}

	/**
	 * Gets feed from given url
	 *
	 * @param  string $feedUrl  Feed Url
	 *
	 * @return array            Feed Content
	 */
	protected function getFeed($feedUrl) {
		$request = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'TYPO3\\CMS\\Core\\Http\\HttpRequest',
			$feedUrl,
			\TYPO3\CMS\Core\Http\HttpRequest::METHOD_GET,
			array(
				'timeout' => 1,
				'follow_redirects' => true
		));
		$result = $request->send();
		$content = $result->getBody();
		$xml = json_decode(json_encode((array) simplexml_load_string($content)), 1);
		return $xml;
	}
}
?>
