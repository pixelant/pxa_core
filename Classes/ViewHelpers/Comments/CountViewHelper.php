<?php
/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

/**
 * ViewHelper to add a like button
 *
 * @package TYPO3
 * @subpackage tx_pxacore
 */
class Tx_PxaCore_ViewHelpers_Comments_CountViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractTagBasedViewHelper {

	/**
	 * @var	integer
	 */
	protected $entryUid = 0;

	/**
	 * @var	Tx_News_Domain_Model_News
	 */
	protected $newsItem = '';

	/**
	 * @var	string
	 */
	protected $settings = '';

	/**
	 * @var	integer
	 */
	protected $pageUid = 0;

	/**
	 * @var	Tx_PwComments_Domain_Repository_CommentRepository
	 * @inject
	 */
	protected $commentRepository;

	/**
	 * @var array
	 */
	protected $detailPidDeterminationCallbacks = array(
		'flexform' => 'getDetailPidFromFlexform',
		'categories' => 'getDetailPidFromCategories',
		'default' => 'getDetailPidFromDefaultDetailPid',
	);


	/**
	 * Arguments initialization
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerUniversalTagAttributes();
		$this->registerArgument('entryUid', 'integer', 'Set record uid');
		$this->registerArgument('newsItem', 'Tx_News_Domain_Model_News', 'NewsItem');
		$this->registerArgument('settings', 'string', 'Object settings');
	}

	/**
	 * Main init method
	 *
	 * @return void
	 */
	protected function init() {
		$this->tag->forceClosingTag(FALSE);
		
		$this->pageUid = $GLOBALS['TSFE']->id;
		$this->entryUid = $this->arguments['entryUid'];
		$this->settings = $this->arguments['settings'];
		$this->newsItem = $this->arguments['newsItem'];
			
			// Special case to get "target" pageUid to get correct count
		if (isset($this->newsItem)) {
			$this->pageUid = $this->determineNewsTargetPid();
		}

	}

	/**
	 * Main render method
	 *
	 * @return string
	 */
	public function render() {
		$this->init();

			// Handle the elementId
		if ($this->entryUid > 0) {
			$comments = $this->commentRepository->findByPidAndEntryUid($this->pageUid, $this->entryUid);
		} else {
			$comments = $this->commentRepository->findByPid($this->pageUid);
		}
		return  count($comments);
	}


	private function determineNewsTargetPid() {

		$detailPid = 0;
		$detailPidDeterminationMethods = t3lib_div::trimExplode(',', $this->settings['detailPidDetermination'], TRUE);

			// if TS is not set, prefer flexform setting
		if (!isset($this->settings['detailPidDetermination'])) {
			$detailPidDeterminationMethods[] = 'flexform';
		}

		foreach ($detailPidDeterminationMethods as $determinationMethod) {
			if ($callback = $this->detailPidDeterminationCallbacks[$determinationMethod]) {
				if ($detailPid = call_user_func(array($this, $callback), $this->settings, $this->newsItem)) {
					break;
				}
			}
		}

		if (!$detailPid) {
			$detailPid = $GLOBALS['TSFE']->id;
		}

		return $detailPid;
	}
	
	/**
	 * Gets detailPid from categories of the given news item. First will be return.
	 *
	 * @param  array $settings
	 * @param  Tx_News_Domain_Model_News $newsItem
	 * @return int
	 */
	protected function getDetailPidFromCategories($settings, $newsItem) {
		$detailPid = 0;
		foreach ($newsItem->getCategories() as $category) {
			if ($detailPid = (int)$category->getSinglePid()) {
				break;
			}
		}
		return $detailPid;
	}

	/**
	 * Gets detailPid from defaultDetailPid setting
	 *
	 * @param  array $settings
	 * @param  Tx_News_Domain_Model_News $newsItem
	 * @return int
	 */
	protected function getDetailPidFromDefaultDetailPid($settings, $newsItem) {
		return (int)$settings['defaultDetailPid'];
	}

	/**
	 * Gets detailPid from flexform of current plugin.
	 *
	 * @param  array $settings
	 * @param  Tx_News_Domain_Model_News $newsItem
	 * @return int
	 */
	protected function getDetailPidFromFlexform($settings, $newsItem) {
		return (int)$settings['detailPid'];

	}

	
}


?>