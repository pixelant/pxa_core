<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Mats Svensson <mats@pixelant.se>, Pixelant AB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

class Tx_PxaCore_ViewHelpers_SiteUrlViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
	
	/**
	 * Iterates through elements of $each and renders child nodes
	 * @param bool $stripTrailingSlash If we should remove trailing slashes from url
	 * @return string
	 */
	public function render($stripTrailingSlash = false) {
		
		$siteUrl = TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL');

		if ($this->arguments['stripTrailingSlash'] == 1) {
			$siteUrl = rtrim($siteUrl, '/');
		}

		return $siteUrl;
	}

}
