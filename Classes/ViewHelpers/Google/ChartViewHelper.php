<?php
/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

/**
 * ViewHelper to add a like button
 * Details: http://developers.facebook.com/docs/reference/plugins/like
 *
 * Examples
 * ==============
 *
 * <n:social.facebook.like />
 * Result: Facebook widget to share the current URL
 *
 * <n:social.facebook.like
 * 		href="http://www.typo3.org"
 * 		width="300"
 * 		font="arial" />
 * Result: Facebook widget to share www.typo3.org within a plugin styled with
 * width 300 and arial as font
 *
 * @package TYPO3
 * @subpackage tx_pxacore
 */
class Tx_PxaCore_ViewHelpers_Google_ChartViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractTagBasedViewHelper {

	/**
	 * @var	string
	 */
	protected $tagName = 'div';

	/**
	 * @var	integer
	 */
	protected static $uid = 0;

	/**
	 * @var	array
	 */
	protected $chartData;

	/**
	 * Arguments initialization
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerUniversalTagAttributes();
		$this->registerArgument('chartData', 'string', 'Given url, if empty, current url is used');
	}

	/**
	 * Main init method
	 *
	 * @return void
	 */
	protected function init() {
		$this->tag->forceClosingTag(TRUE);
		$this->chartData = $this->arguments['chartData'];
	}

	/**
	 * Main render method
	 *
	 * @return string
	 */
	public function render() {
		$this->init();

		// Handle the elementId
		if (!empty($this->arguments['id']) || !empty($this->chartData['containerId'])) {
			if (!empty($this->arguments['id'])) {
				$this->tag->addAttribute('id', $this->arguments['id']);
			} else {
				$this->tag->addAttribute('id', $this->chartData['containerId']);
			}
		} else {
			// Create new unique Id
			$this->tag->addAttribute('id', 'gChart' . self::$uid++);
		}
		$this->chartData['containerId'] = $this->tag->getAttribute('id');

		$code = t3lib_div::wrapJS('
			pxa.charts.addChart(' . json_encode($this->chartData, JSON_NUMERIC_CHECK) . ');
			');
		
		return $this->tag->render() . $code;
	}

}

?>