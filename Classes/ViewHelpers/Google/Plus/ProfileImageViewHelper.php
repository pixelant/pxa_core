<?php
// namespace Tx_PxaCore\ViewHelpers;

/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

class Tx_PxaCore_ViewHelpers_Google_Plus_ProfileImageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

	/**
	 * @var string
	 */
	protected $tagName = 'img';

	/**
	 * @var \Pixelant\PxaCore\Domain\Repository\BackendUserRepository
	 * @inject
	 */
	protected $backendUserRepository;


	/**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerTagAttribute('email', 'string', 'The email of the be_user to find google+ profile id for.', TRUE);
		$this->registerTagAttribute('size', 'integer', 'The size of the profile image attribute.', FALSE);
		$this->registerTagAttribute('class', 'string', 'The class attribute.', FALSE);
	}

	/**
	 * Render google author rel tag "<a rel="author" href="https://plus.google.com/u/0/xxxxxxxxxxx?rel=author" />
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string the a tag with rel if be_user was with email was found
	 */
	public function render() {

			// Variables	
		$imageSize = (int)$this->arguments['email'] > 0 ? (int)$this->arguments['email'] : 64;

			// Try to find a BE user with email
 		$beUser = $this->backendUserRepository->findOneByEmail($this->arguments['email']);

 			// If found a BE user
 		if (isset($beUser)) {
 				// Get ProfileID
 			$googlePlusProfileId = $beUser->getGooglePlusProfileId();

 				// If user has ProfileId
 			if (strlen($googlePlusProfileId) > 0) {

 					// Add attributes
				$this->tag->addAttribute('src', 'https://plus.google.com/s2/photos/profile/' . $googlePlusProfileId . '?sz=' . $imageSize);
 			}
 		}
 		
 			// Remove email attribute
 		$this->tag->removeAttribute('email');
 		$this->tag->removeAttribute('size');
		
 		return $this->tag->render();
	}
}

?>