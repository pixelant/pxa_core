<?php
/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */

class Tx_PxaCore_ViewHelpers_LinkViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

	/**
	 * @var string
	 */
	protected $tagName = 'a';

	/**
	 * @param integer|NULL $link target page. See TypoLink destination
	 * @param array $additionalParams query parameters to be attached to the resulting URI
	 * @param integer $pageType type of the target page. See typolink.parameter
	 * @param boolean $noCache set this to disable caching for the target page. You should not need this.
	 * @param boolean $noCacheHash set this to supress the cHash query parameter created by TypoLink. You should not need this.
	 * @param string $section the anchor to be added to the URI
	 * @param boolean $linkAccessRestrictedPages If set, links pointing to access restricted pages will still link to the page even though the page cannot be accessed.
	 * @param boolean $absolute If set, the URI of the rendered link is absolute
	 * @param boolean $addQueryString If set, the current query parameters will be kept in the URI
	 * @param array $argumentsToBeExcludedFromQueryString arguments to be removed from the URI. Only active if $addQueryString = TRUE
	 * @return string Rendered page URI
	 */
	public function render($link = NULL, array $additionalParams = array(), $pageType = 0, $noCache = FALSE, $noCacheHash = FALSE, $section = '', $linkAccessRestrictedPages = FALSE, $absolute = FALSE, $addQueryString = FALSE, array $argumentsToBeExcludedFromQueryString = array()) {

		if(\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($link)) {
			
			// Internal pages
			$uriBuilder = $this->controllerContext->getUriBuilder();
			$uri = $uriBuilder->reset()->setTargetPageUid($link)->setTargetPageType($pageType)->setNoCache($noCache)->setUseCacheHash(!$noCacheHash)->setSection($section)->setLinkAccessRestrictedPages($linkAccessRestrictedPages)->setArguments($additionalParams)->setCreateAbsoluteUri($absolute)->setAddQueryString($addQueryString)->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)->build();
			$this->tag->addAttribute('href', $uri);
			$this->tag->setContent($this->renderChildren());
			return $this->tag->render();

		} else if(strpos($link, 'file:') === 0) {

			// Link to file
			$uriBuilder = $this->controllerContext->getUriBuilder();
			$uri = $uriBuilder->reset()->setTargetPageUid($link)->setTargetPageType($pageType)->setNoCache($noCache)->setUseCacheHash(!$noCacheHash)->setSection($section)->setLinkAccessRestrictedPages($linkAccessRestrictedPages)->setArguments($additionalParams)->setCreateAbsoluteUri($absolute)->setAddQueryString($addQueryString)->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)->build();
			$this->tag->addAttribute('href', $uri);
			$this->tag->setContent($this->renderChildren());
			return $this->tag->render();

		} else if(\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($link) === TRUE) {

			// E-mail
			//$emailViewHelper = t3lib_div::makeInstance('TYPO3\\CMS\\Fluid\\ViewHelpers\\Link\\EmailViewHelper');
			//return $emailViewHelper->render($link));

			if (TYPO3_MODE === 'FE') {
				list($linkHref, $linkText) = $GLOBALS['TSFE']->cObj->getMailTo($link, $link);
			} else {
				$linkHref = 'mailto:' . $email;
				$linkText = $email;
			}
			$tagContent = $this->renderChildren();
			if ($tagContent !== NULL) {
				$linkText = $tagContent;
			}
			$this->tag->setContent($this->renderChildren());
			$escapeSpecialCharacters = !isset($GLOBALS['TSFE']->spamProtectEmailAddresses) || $GLOBALS['TSFE']->spamProtectEmailAddresses !== 'ascii';
			$this->tag->addAttribute('href', $linkHref, $escapeSpecialCharacters);
			$this->tag->forceClosingTag(TRUE);
			return $this->tag->render();

		} else {

			// External page
			//$externalViewHelper = t3lib_div::makeInstance('\\TYPO3\\CMS\\Fluid\\ViewHelpers\\Link\\ExternalViewHelper');
			//return $externalViewHelper->render($link);

			$scheme = parse_url($link, PHP_URL_SCHEME);
			$defaultScheme = 'http';
			if ($scheme === NULL && $defaultScheme !== '') {
				$link = $defaultScheme . '://' . $link;
			}
			$this->tag->addAttribute('href', $link);
			$this->tag->addAttribute('target', '_blank');
			$this->tag->setContent($this->renderChildren());
			$this->tag->forceClosingTag(TRUE);

			return $this->tag->render();
		}


	}
}

?>
