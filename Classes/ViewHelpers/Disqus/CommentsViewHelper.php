<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Mats Svensson <mats@pixelant.se>, Pixelant AB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

class Tx_PxaCore_ViewHelpers_Disqus_CommentsViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
	
	/**
	 * Renders js to create Disqus Comments
	 * @param string $disqusShort NameDisqus shortname
	 * @param string $disqusIdentifier Disqus Identifier
	 * @param string $disqusUrl Disqus URL
	 * @param string $disqusTitle Disqus Title
	 * @param string $disqusLanguage Disqus Language
	 * @return string
	 */
	public function render($disqusShortName, $disqusIdentifier, $disqusUrl, $disqusTitle, $disqusLanguage) {
		
		$html = '<div id="disqus_thread"></div>';
		$jsCode = '<script type="text/javascript">
					var disqus_shortname = ' . TYPO3\CMS\Core\Utility\GeneralUtility::quoteJSvalue($disqusShortName) . ';
					var disqus_identifier = ' . TYPO3\CMS\Core\Utility\GeneralUtility::quoteJSvalue($disqusIdentifier) . ';
					var disqus_url = ' . TYPO3\CMS\Core\Utility\GeneralUtility::quoteJSvalue($disqusUrl) . ';
					var disqus_title = ' . TYPO3\CMS\Core\Utility\GeneralUtility::quoteJSvalue($disqusTitle) . ';
					var disqus_config = function () {
						this.language = ' . TYPO3\CMS\Core\Utility\GeneralUtility::quoteJSvalue($disqusLanguage) . ';
					};

					(function() {
						var dsq = document.createElement("script"); dsq.type = "text/javascript"; dsq.async = true;
						dsq.src = "//" + disqus_shortname + ".disqus.com/embed.js";
						(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(dsq);
					})();
				</script>
				<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>';
				
		return $html . $jsCode;
	}

}
