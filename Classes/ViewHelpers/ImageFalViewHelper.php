<?php
// namespace Tx_PxaCore\ViewHelpers;

/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */
/**
 * Resizes a given image (if required) and renders the respective img tag
 *
 * = Examples =
 *
 * <code title="Default">
 * <f:image src="EXT:myext/Resources/Public/typo3_logo.png" alt="alt text" />
 * </code>
 * <output>
 * <img alt="alt text" src="typo3conf/ext/myext/Resources/Public/typo3_logo.png" width="396" height="375" />
 * or (in BE mode):
 * <img alt="alt text" src="../typo3conf/ext/viewhelpertest/Resources/Public/typo3_logo.png" width="396" height="375" />
 * </output>
 *
 * <code title="Inline notation">
 * {f:image(src: 'EXT:viewhelpertest/Resources/Public/typo3_logo.png', alt: 'alt text', minWidth: 30, maxWidth: 40)}
 * </code>
 * <output>
 * <img alt="alt text" src="../typo3temp/pics/f13d79a526.png" width="40" height="38" />
 * (depending on your TYPO3s encryption key)
 * </output>
 *
 * <code title="non existing image">
 * <f:image src="NonExistingImage.png" alt="foo" />
 * </code>
 * <output>
 * Could not get image resource for "NonExistingImage.png".
 * </output>
 */
class Tx_PxaCore_ViewHelpers_ImageFalViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

	/**
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
	 */
	protected $contentObject;

	/**
	 * @var string
	 */
	protected $tagName = 'img';

	/**
	 * @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController contains a backup of the current $GLOBALS['TSFE'] if used in BE mode
	 */
	protected $tsfeBackup;

	/**
	 * @var string
	 */
	protected $workingDirectoryBackup;

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
	 * @return void
	 */
	public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
		$this->contentObject = $this->configurationManager->getContentObject();
	}

	/**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerTagAttribute('alt', 'string', 'Specifies an alternate text for an image', TRUE);
		$this->registerTagAttribute('ismap', 'string', 'Specifies an image as a server-side image-map. Rarely used. Look at usemap instead', FALSE);
		$this->registerTagAttribute('longdesc', 'string', 'Specifies the URL to a document that contains a long description of an image', FALSE);
		$this->registerTagAttribute('usemap', 'string', 'Specifies an image as a client-side image-map', FALSE);
		$this->registerTagAttribute('aspectRatio', 'string', 'Specifies the image aspect ratio', FALSE);
		$this->registerTagAttribute('cropWidth', 'int', '', FALSE);
		$this->registerTagAttribute('cropY', 'int', '', FALSE);
		$this->registerTagAttribute('cropX', 'int', '', FALSE);
		$this->registerTagAttribute('bePreview', 'int', '', FALSE);
	}

	/**
	 * Resizes a given image (if required) and renders the respective img tag
	 *
	 * @see http://typo3.org/documentation/document-library/references/doc_core_tsref/4.2.0/view/1/5/#id4164427
	 * @param array $src
	 * @param string $width width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
	 * @param string $height height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
	 * @param integer $minWidth minimum width of the image
	 * @param integer $minHeight minimum height of the image
	 * @param integer $maxWidth maximum width of the image
	 * @param integer $maxHeight maximum height of the image
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string rendered tag.
	 */
	public function render($src, $width = NULL, $height = NULL, $minWidth = NULL, $minHeight = NULL, $maxWidth = NULL, $maxHeight = NULL) {
		if (TYPO3_MODE === 'BE') {
			$this->simulateFrontendEnvironment();
		}
		
		$setup = array(
			'width' => $width,
			'height' => $height,
			'minW' => $minWidth,
			'minH' => $minHeight,
			'maxW' => $maxWidth,
			'maxH' => $maxHeight,
			// 'params' => '-rotate 180 '
			// 'params' => ' -crop ' . $width . 'x'. $height . '+0+0 '
		);

		// Handle additional param aspectRatio
		list($aspectRatioWidth,$aspectRatioHeight) = explode(":", $this->arguments['aspectRatio']);

		if($aspectRatioWidth > 0 && $aspectRatioHeight > 0) {
			$factor = $aspectRatioWidth / $aspectRatioHeight;
			$height = ceil($width / $factor);

			// Only need to do the crop and resize by param if there is a "crop-selection" made
			if (intval($this->arguments['cropWidth']) > 0) {
				$cropWidth = intval($this->arguments['cropWidth']);
				$cropHeight = ceil($cropWidth / $factor);
				$cropY = intval($this->arguments['cropY']);
				$cropX = intval($this->arguments['cropX']);

				unset($setup);

				$setup['params'] = " -crop " . $cropWidth . "x" . $cropHeight ."+" . $cropX . "+" . $cropY . "  -resize " . $width . "x" . $height ." ";
			} else {
				$setup['width'] = $width;
				$setup['height'] = $height;
			}
		}
		
		/*
		$src['original']['width']
		$src['original']['height']
		$src['original']['storage']
		$src['original']['identifier']
		*/

		$originalPath = '';
		switch ($src['original']['storage']) {
			case 0:
				$originalPath = 'uploads' . $src['original']['identifier'];
				break;
			default:
				$originalPath = 'fileadmin' . $src['original']['identifier'];
				break;
		}
		
		$imageInfo = $this->contentObject->getImgResource($originalPath, $setup);

		$GLOBALS['TSFE']->lastImageInfo = $imageInfo;
		if (!is_array($imageInfo)) {
			if (TYPO3_MODE === 'BE') {
				$this->resetFrontendEnvironment();
			}
			throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception('Could not get image resource for "' . htmlspecialchars($originalPath) . '".', 1253191060);
		}
		$imageInfo[3] = \TYPO3\CMS\Core\Utility\GeneralUtility::png_to_gif_by_imagemagick($imageInfo[3]);
		
		$GLOBALS['TSFE']->imagesOnPage[] = $imageInfo[3];
		$imageSource = $GLOBALS['TSFE']->absRefPrefix . \TYPO3\CMS\Core\Utility\GeneralUtility::rawUrlEncodeFP($imageInfo[3]);
		if (TYPO3_MODE === 'BE') {
			if (intval($this->arguments['bePreview']) > 0 ) {
				// Just to make images visible in BE "Page" view...
				$imageSource = '../../../../' . $imageSource;
				$this->resetFrontendEnvironment();	
			} else {
				$imageSource = '../' . $imageSource;
				$this->resetFrontendEnvironment();	
			}			
		}
		$this->tag->addAttribute('src', $imageSource);
		$this->tag->addAttribute('width', $imageInfo[0]);
		$this->tag->addAttribute('height', $imageInfo[1]);

		// Remove special tags
		$this->tag->removeAttribute('aspectRatio');
		$this->tag->removeAttribute('cropWidth');
		$this->tag->removeAttribute('cropY');
		$this->tag->removeAttribute('cropX');
		$this->tag->removeAttribute('bePreview');

		//the alt-attribute is mandatory to have valid html-code, therefore add it even if it is empty
		if (empty($this->arguments['alt'])) {
			$this->tag->addAttribute('alt', $src['reference']['alternative']);
		}
		if (empty($this->arguments['title']) && !empty($src['reference']['title'])) {
			$this->tag->addAttribute('title', $src['reference']['title']);
		}
		return $this->tag->render();
	}

	/**
	 * Prepares $GLOBALS['TSFE'] for Backend mode
	 * This somewhat hacky work around is currently needed because the getImgResource() function of tslib_cObj relies on those variables to be set
	 *
	 * @return void
	 */
	protected function simulateFrontendEnvironment() {
		$this->tsfeBackup = isset($GLOBALS['TSFE']) ? $GLOBALS['TSFE'] : NULL;
		// set the working directory to the site root
		$this->workingDirectoryBackup = getcwd();
		chdir(PATH_site);
		$typoScriptSetup = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
		$GLOBALS['TSFE'] = new \stdClass();
		$template = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\TypoScript\\TemplateService');
		$template->tt_track = 0;
		$template->init();
		$template->getFileName_backPath = PATH_site;
		$GLOBALS['TSFE']->tmpl = $template;
		$GLOBALS['TSFE']->tmpl->setup = $typoScriptSetup;
		$GLOBALS['TSFE']->config = $typoScriptSetup;
	}

	/**
	 * Resets $GLOBALS['TSFE'] if it was previously changed by simulateFrontendEnvironment()
	 *
	 * @return void
	 * @see simulateFrontendEnvironment()
	 */
	protected function resetFrontendEnvironment() {
		$GLOBALS['TSFE'] = $this->tsfeBackup;
		chdir($this->workingDirectoryBackup);
	}
}

?>