<?php
namespace Pixelant\PxaCore\Hooks;

class userTqSeoHooks {
	public static function hook_pagetitleSetup(&$args, $obj) {
		$rawTitle = $GLOBALS['TSFE']->page['title'];
		$extraTitle = '';
		$extraTitleGlue = ' ';
		$extraTitleCurrentPageSeparator = '-';
		// Check if news list tag, category,date or pagination is active
		if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('news')) {
			$gpArguments = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_news_pi1');
			if (is_array($gpArguments)) {
				foreach ($gpArguments as $rootKey => $values) {
					if (is_array($values)) {
						foreach ($values as $parameter => $value) {
							if ($parameter == 'categories') {
								$categoryRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('GeorgRinger\\News\\Domain\\Repository\\CategoryRepository');
								$category = $categoryRepository->findByUid((int)$value);
								if ($category) {
									$extraTitle .= strlen($extraTitle) > 0 ? $extraTitleGlue : '';
									$extraTitle .= $category->getTitle();
								}
							}
							if ($parameter == 'year') {
								if ((int)$value > 0) {
									$extraTitle .= strlen($extraTitle) > 0 ? $extraTitleGlue : '';
									$extraTitle .= $extraTitleCurrentPageSeparator . $extraTitleGlue . $value;
								}
							}
							if ($parameter == 'month') {
								if ((int)$value > 0) {
									$extraTitle .= strlen($extraTitle) > 0 ? $extraTitleGlue : '';
									$extraTitle .= $extraTitleCurrentPageSeparator . $extraTitleGlue . $value;
								}
							}
							if ($parameter == 'tags') {
								$tagRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('GeorgRinger\\News\\Domain\\Repository\\TagRepository');
								$tag = $tagRepository->findByUid((int)$value);
								if ($tag) {
									$extraTitle .= strlen($extraTitle) > 0 ? $extraTitleGlue : '';
									$extraTitle .= $tag->getTitle();
								}
							}
							if ($parameter == 'currentPage') {
								if ((int)$value > 0) {
									$pageText = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('hook.userTqSeoHooks.page', 'pxa_core', NULL);
									$extraTitle .= strlen($extraTitle) > 0 ? $extraTitleGlue : '';
									$extraTitle .= $extraTitleCurrentPageSeparator . $extraTitleGlue . $pageText . $extraTitleGlue . $value;
								}
							}
						}
					}
				}
			}
		}
		if (strlen($extraTitle) > 0) {
			$connector = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TQ\\TqSeo\\Connector');
			$connector->setPageTitle($rawTitle . $extraTitleGlue . $extraTitle);	
		}
	}
}