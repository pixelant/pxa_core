
<!-- <script src="../typo3conf/ext/pxa_çore/Resources/Public/js/jquery-1.8.2.min.js" type="text/javascript"></script> -->
<script type="text/javascript">
	var jQuery = jQuery || TYPO3.jQuery;
</script>
<script src="../typo3conf/ext/pxa_core/Resources/Public/Js/Backend/jquery.Jcrop.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../typo3conf/ext/pxa_crop/Resources/Public/Css/Backend/jquery.Jcrop.min.css" media="all">


<?php

/**
 * Renders 
 *
 * @package	PxaCore
 * @subpackage UserFunction
 */
class Tx_PxaCore_UserFunction_Crop {
	
	/**
	 * @var Tx_Extbase_Object_ObjectManager
	 */
	protected $objectManager;

	/**
	 * @var Tx_Flux_Service_FluxService $fluxService
	 */
	protected $fluxService;

	/**
	 * @var array The array flexform
	 */
	protected $flexform = array();

	/**
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
	 */
	protected $contentObject;

	/**
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController contains a backup of the current $GLOBALS['TSFE'] if used in BE mode
	 */
	protected $tsfeBackup;

	/**
	 * @var string The name of the image field in flexform
	 */
	protected $imageField;

	/**
	 * @var string The type of image in flexform (fal or file for now)
	 */
	protected $imageType;

	/**
	 * @var string The name of the sheet with crop data
	 */
	protected $formSheetName;

	/**
	 * @var int The width of the preview crop image in flexform
	 */
	protected $previewImageWidth;

	/**
	 * @var int The width of the original image
	 */
	protected $originalImageWidth;

	/**
	 * @var int The height of the original image
	 */
	protected $originalImageHeight;

	/**
	* @var string Original image path
	*/
	protected $originalImageSource;

	/**
	 * @var int The image scale factor
	 */
	protected $imageScaleFactor;
	
	/**
	 * @var string The aspect ratio string :
	 */
	protected $aspectRatio;

	/**
	 * @var int The aspect ratio width
	 */
	protected $aspectRatioWidth;
	
	/**
	 * @var int The aspect ratio height
	 */
	protected $aspectRatioHeight;

	/**
	 * @var int The factor to calculate crop values from
	 */
	protected $factor;

	/**
	 * @var int The uid of the content
	 */
	protected $uid;

	/**
	 * @var int The cropY
	 */
	protected $cropY;

	/**
	 * @var int The cropX
	 */
	protected $cropX;

	/**
	 * @var int The cropY2
	 */
	protected $cropY2;

	/**
	 * @var int The cropX2
	 */
	protected $cropX2;

	/**
	 * @var string The actual content
	 */
	protected $content;

	/**
	 * @param array $parameters
	 * @param t3lib_TCEforms $pObj
	 * @return string
	 */
	public function renderField(array $parameters,t3lib_TCEforms $pObj) {
			
		$this->initObjects($parameters);
		
		$this->setOriginalImageData($parameters);

		$this->imageScaleFactor = $this->previewImageWidth / $this->originalImageWidth;

		$this->setAspectRatioAndFactor();

		$this->setCroppingValues();

		$cropImage = $this->getCropImage();

			// Get the url to the second image (scaled) to use in flexform
		$imageSource = $GLOBALS['TSFE']->absRefPrefix . \TYPO3\CMS\Core\Utility\GeneralUtility::rawUrlEncodeFP($cropImage[3]);
		if (TYPO3_MODE === 'BE') {
			$imageSource = '../' . $imageSource;
			$this->resetFrontendEnvironment();
		}

			// Add the image to the content
		$this->content.= '<img id="jcrop_target" style="width:'. $this->previewImageWidth . 'px;" src="' . $imageSource .'" alt="cropimage"/>';
		
			// Add filename below image
		$this->content.= '<p style="width:'. $this->previewImageWidth . 'px;text-align:center;">' . $cropImage[3] . '<br />(' . $this->originalImageSource . ')</p>';

		$this->addJavaScriptToContent();
		
		return $this->content;
	}

	/**
	 * Prepares $GLOBALS['TSFE'] for Backend mode
	 * This somewhat hacky work around is currently needed because the getImgResource() function of tslib_cObj relies on those variables to be set
	 *
	 * @return void
	 */
	protected function simulateFrontendEnvironment() {
		$this->tsfeBackup = isset($GLOBALS['TSFE']) ? $GLOBALS['TSFE'] : NULL;
		// set the working directory to the site root
		$this->workingDirectoryBackup = getcwd();
		chdir(PATH_site);
		$typoScriptSetup = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
		$GLOBALS['TSFE'] = new \stdClass();
		$template = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\TypoScript\TemplateService');
		$template->tt_track = 0;
		$template->init();
		$template->getFileName_backPath = PATH_site;
		$GLOBALS['TSFE']->tmpl = $template;
		$GLOBALS['TSFE']->tmpl->setup = $typoScriptSetup;
		$GLOBALS['TSFE']->config = $typoScriptSetup;
	}

	/**
	 * Resets $GLOBALS['TSFE'] if it was previously changed by simulateFrontendEnvironment()
	 *
	 * @return void
	 * @see simulateFrontendEnvironment()
	 */
	protected function resetFrontendEnvironment() {
		$GLOBALS['TSFE'] = $this->tsfeBackup;
		chdir($this->workingDirectoryBackup);
	}

	/**
	* Initializes objects
	* @return void
	*
	*/
	protected function initObjects($parameters) {
		
			// Create ObjectManager
		$this->objectManager = t3lib_div::makeInstance('Tx_Extbase_Object_ObjectManager');
		
			// Get flexform
		$this->fluxService = $this->objectManager->get('Tx_Flux_Service_FluxService');

			// get configurationmanager and contentObject
		$this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
		$this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$this->contentObject = $this->configurationManager->getContentObject();
		if ($this->contentObject === NULL) {
			$this->contentObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
		}

			// If BE simulate FrontendEnvironment, copied from flux ImageViewHelper
		if (TYPO3_MODE === 'BE') {
			$this->simulateFrontendEnvironment();
		}

			// Fetch params from flexform
		$this->uid = $parameters['row']['uid'];

			// parse flexform data to an array
		$this->flexform = $this->fluxService->convertFlexFormContentToArray($parameters['row']['pi_flexform']);
				
			// Set the width of the image in the flexform, used to calculate other values in form
		$this->previewImageWidth = isset($parameters['fieldConf']['config']['default']['previewImageWidth']) ? $parameters['fieldConf']['config']['default']['previewImageWidth'] : '600';;
		$this->formSheetName = isset($parameters['fieldConf']['config']['default']['sheetNameCrop']) ? $parameters['fieldConf']['config']['default']['sheetNameCrop'] : 'sheetCrop';

			// Get image type and field-name from the "default" param as array
		$this->imageField = isset($parameters['fieldConf']['config']['default']['imageField']) ? $parameters['fieldConf']['config']['default']['imageField'] : 'file';
		$this->imageType = isset($parameters['fieldConf']['config']['default']['imageType']) ? $parameters['fieldConf']['config']['default']['imageType'] : 'file';

	}

	/**
	* Sets original image data
	* @return void
	*
	*/
	protected function setOriginalImageData($parameters) {

		switch ($this->imageType) {
			case 'fal':
				$this->setOriginalImageDataFromFal($parameters);
				break;
			default:
				$this->setOriginalImageDataFromFile();
				break;
		}

	}
	
	/**
	* Sets original image data for FAL images
	* @return void
	*
	*/
	protected function setOriginalImageDataFromFal($parameters) {
		
		$fileRepository = new \TYPO3\CMS\Core\Resource\FileRepository();
		$fileObjects = $fileRepository->findByRelation('tt_content', $this->imageField , $parameters['row']['uid']);
		
			// Loop thru all images, but for now only first is used	
		foreach ($fileObjects as $key => $value) {
			$files[$key]['reference'] = $value->getReferenceProperties();
			$files[$key]['original'] = $value->getOriginalFile()->getProperties();
		}

		switch ($files[0]['original']['storage']) {
			case 0:
				$src = 'uploads' . $files[0]['original']['identifier'];
				break;
			default:
				$src = 'fileadmin' . $files[0]['original']['identifier'];
				break;
		}

			// Check original image info, and calculate imageScaleFactor
		$this->originalImageWidth = $files[0]['original']['width'];
		$this->originalImageHeight = $files[0]['original']['height'];
		$this->originalImageSource = $GLOBALS['TSFE']->absRefPrefix . \TYPO3\CMS\Core\Utility\GeneralUtility::rawUrlEncodeFP($src);
	
	}

	/**
	* Sets original image data for file images
	* @return void
	*
	*/
	protected function setOriginalImageDataFromFile() {

		$src = $this->flexform[$this->imageField];
		$imageInfo =  $this->contentObject->getImgResource($src, $setup);
		$this->originalImageWidth = $imageInfo[0];
		$this->originalImageHeight = $imageInfo[1];
		$this->originalImageSource = $GLOBALS['TSFE']->absRefPrefix . \TYPO3\CMS\Core\Utility\GeneralUtility::rawUrlEncodeFP($imageInfo[3]);

	}

	/**
	* Sets aspect ratio and factor
	* @return void
	*
	*/
	protected function setAspectRatioAndFactor() {

			// Fetch flexform aspectration
		$this->aspectRatio = str_replace(":", "/", $this->flexform["aspectRatio"]);
		list($this->aspectRatioWidth,$this->aspectRatioHeight) = explode(":", $this->flexform["aspectRatio"]);

			// Calculate aspectRatio factor from supplied aspect ratio
		if($this->aspectRatioWidth > 0 && $this->aspectRatioHeight > 0) {
			$this->factor = $this->aspectRatioWidth / $this->aspectRatioHeight;
		}		
	}

	/**
	* Ǵets a the cropped image
	* @return void
	*
	*/
	protected function getCropImage() {

			// Fetch image for flexform with dimensions based on the imageScaleFacor from the original file
		$setup = array(
			'width' => $this->originalImageWidth * $this->imageScaleFactor, 
			'height' => $this->originalImageHeight * $this->imageScaleFactor, 
		);
		return $this->contentObject->getImgResource($this->originalImageSource, $setup);
	}

	/**
	* Sets the croppint values
	* @return void
	*
	*/
	protected function setCroppingValues() {
			// Calculate x,y,x2 and y2 for the jCrop:  top, left, width, height
		$this->cropY = intval($this->flexform["cropY"] * $this->imageScaleFactor);
		$this->cropX = intval($this->flexform["cropX"] * $this->imageScaleFactor);
		$this->cropX2 = intval(($this->flexform["cropX"] + $this->flexform["cropWidth"])  * $this->imageScaleFactor);
		$this->cropY2 = ceil($this->cropX2 / $this->factor);

	}

	/**
	* Adds js to content variable
	* @return void
	*
	*/
	protected function addJavaScriptToContent() {
	
			// Add the javascript for jcrop, create object, onchange function and update form inputs.
		$this->content.= "
		<script type=\"text/javascript\">
			var factor = " . $this->imageScaleFactor . ";
			TYPO3.jQuery(document).ready(function() {

				TYPO3.jQuery('#jcrop_target').Jcrop({
		            onChange:    showCoords,
		            bgColor:     'black',
		            bgOpacity:   .4,
			        setSelect:   [ " . $this->cropX . "," . $this->cropY . "," . $this->cropX2 . "," . $this->cropY2 . "],
		            aspectRatio: " . $this->aspectRatio . "
		        });
			});
	
			function showCoords(c)
			{
				// variables can be accessed here as
				// c.x, c.y, c.x2, c.y2, c.w, c.h
				TYPO3.jQuery('[name=\"data[tt_content][" . $this->uid . "][pi_flexform][data][" . $this->formSheetName . "][lDEF][cropWidth][vDEF]\"]').val(parseInt(c.w / factor));
				TYPO3.jQuery('[name=\"data[tt_content][" . $this->uid . "][pi_flexform][data][" . $this->formSheetName . "][lDEF][cropX][vDEF]\"]').val(parseInt(c.x / factor));
				TYPO3.jQuery('[name=\"data[tt_content][" . $this->uid . "][pi_flexform][data][" . $this->formSheetName . "][lDEF][cropY][vDEF]\"]').val(parseInt(c.y / factor));				
			};
		</script>";		
	}

	/**
	* Adds debuginfo to content variable
	* @return void
	*
	*/
	protected function addDebugToContent() {

		$this->content.="previewImageWidth = " . $this->previewImageWidth . "<br />";
		$this->content.="originalImageWidth = " . $this->originalImageWidth . "<br />";
		$this->content.="originalImageHeight = " . $this->originalImageHeight . "<br />";
		$this->content.="originalImageSource = " . $this->originalImageSource . "<br />";
		$this->content.="imageScaleFactor = " . $this->imageScaleFactor . "<br />";
		$this->content.="aspectRatioWidth = " . $this->aspectRatioWidth . "<br />";
		$this->content.="aspectRatioHeight = " . $this->aspectRatioHeight . "<br />";
		$this->content.="factor = " . $this->factor . "<br />";
		$this->content.="cropY = " . $this->cropY . "<br />";
		$this->content.="cropX = " . $this->cropX . "<br />";
		$this->content.="cropY2 = " . $this->cropY2 . "<br />";
		$this->content.="cropX2 = " . $this->cropX2 . "<br />";

	}


}

?>