<?php

if (!defined ('TYPO3_MODE'))
	die('Access denied.');


// Add our page ts config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:pxa_core/Configuration/TypoScript/pageTsConfig.ts">');
// Add our user ts config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:pxa_core/Configuration/TypoScript/userTsConfig.ts">');

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['Tx_News_Interfaces_Video_Videosites'] = array('className' => 'Tx_PxaCore_Interfaces_Videosites');
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Backend\\Controller\\LoginController'] = array('className' => 'Pixelant\\PxaCore\\Controller\\LoginController');
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tq_seo']['hooks']['pagefooter-setup'][] = 'Pixelant\\PxaCore\\Hooks\\userTqSeoHooks->hook_pagetitleSetup';
?>